package silbertb.protodomainconverter.usage.domain;


import lombok.Builder;
import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.model.proto.TimestampProto;

import java.time.Instant;

@Builder
@Value
@ProtoBuilder
@ProtoClass(protoClass = TimestampProto.class, blacklist = true)
public class TimestampDomain {
    Instant timestamp;
}
