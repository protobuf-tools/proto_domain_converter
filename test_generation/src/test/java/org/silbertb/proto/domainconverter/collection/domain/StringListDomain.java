package org.silbertb.proto.domainconverter.collection.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;
import org.silbertb.proto.domainconverter.test.proto.collection.StringListProto;

import java.util.List;

@Data
@ProtoClass(protoClass = StringListProto.class)
public class StringListDomain {

    @ProtoField
    private List<String> stringList;
}
