package org.silbertb.proto.domainconverter.collection.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;
import org.silbertb.proto.domainconverter.test.proto.collection.PrimitiveMapProto;

import java.util.Map;

@Data
@ProtoClass(protoClass = PrimitiveMapProto.class)
public class PrimitiveMap {

    @ProtoField
    private Map<Integer, Long> primitiveMap;
}
