package org.silbertb.proto.domainconverter.oneof;

import org.junit.jupiter.api.Test;

import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.bean.OneofSegmentDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.class_builder.OneofSegmentBuilderDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.constructor.OneofSegmentConstructorDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.constructor_builder.OneofSegmentConstructorBuilderDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.no_members.ShapeDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_field.OneofSegmentFieldDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_impl_hierarchy.domain_class.OneofImplHierarchyDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_impl_hierarchy.domain_field.OneofImplHierarchyFieldDomain;
import org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain.OneofDefinedFromFieldAnnotationBuilderDomain;
import org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain.OneofDefinedFromFieldAnnotationPojoDomain;
import org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain.OneofOneImplSeveralFieldsDomain;
import org.silbertb.proto.domainconverter.test.proto.oneof.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OneofTest {
    @Test
    void testOneofOnClassBeanPrimitiveToProto() {
        OneofSegmentDomain domain = OneofTestObjectsCreator.createOneofSegmentPointDomain();
        OneofSegmentProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentProto expected = OneofTestObjectsCreator.createOneofSegmentPointProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBeanPrimitiveToDomain() {
        OneofSegmentProto proto = OneofTestObjectsCreator.createOneofSegmentPointProto();
        OneofSegmentDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentDomain expected = OneofTestObjectsCreator.createOneofSegmentPointDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBeanMessageToProto() {
        OneofSegmentDomain domain = OneofTestObjectsCreator.createOneofSegmentOpenRangeDomain();
        OneofSegmentProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentProto expected = OneofTestObjectsCreator.createOneofSegmentOpenRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBeanMessageToDomain() {
        OneofSegmentProto proto = OneofTestObjectsCreator.createOneofSegmentOpenRangeProto();
        OneofSegmentDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentDomain expected = OneofTestObjectsCreator.createOneofSegmentOpenRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBeanMapperToProto() {
        OneofSegmentDomain domain = OneofTestObjectsCreator.createOneofSegmentRangeDomain();
        OneofSegmentProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentProto expected = OneofTestObjectsCreator.createOneofSegmentRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBeanMapperToDomain() {
        OneofSegmentProto proto = OneofTestObjectsCreator.createOneofSegmentRangeProto();
        OneofSegmentDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentDomain expected = OneofTestObjectsCreator.createOneofSegmentRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBuilderPrimitiveToProto() {
        OneofSegmentBuilderDomain domain = OneofTestObjectsCreator.createOneofSegmentBuilderPointDomain();
        OneofSegmentBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentBuilderProto expected = OneofTestObjectsCreator.createOneofSegmentBuilderPointProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBuilderPrimitiveToDomain() {
        OneofSegmentBuilderProto proto = OneofTestObjectsCreator.createOneofSegmentBuilderPointProto();
        OneofSegmentBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentBuilderDomain expected = OneofTestObjectsCreator.createOneofSegmentBuilderPointDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBuilderMessageToProto() {
        OneofSegmentBuilderDomain domain = OneofTestObjectsCreator.createOneofSegmentBuilderOpenRangeDomain();
        OneofSegmentBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentBuilderProto expected = OneofTestObjectsCreator.createOneofSegmentBuilderOpenRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBuilderMessageToDomain() {
        OneofSegmentBuilderProto proto = OneofTestObjectsCreator.createOneofSegmentBuilderOpenRangeProto();
        OneofSegmentBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentBuilderDomain expected = OneofTestObjectsCreator.createOneofSegmentBuilderOpenRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassBuilderMapperToProto() {
        OneofSegmentBuilderDomain domain = OneofTestObjectsCreator.createOneofSegmentBuilderRangeDomain();
        OneofSegmentBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentBuilderProto expected = OneofTestObjectsCreator.createOneofSegmentBuilderRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassBuilderMapperToDomain() {
        OneofSegmentBuilderProto proto = OneofTestObjectsCreator.createOneofSegmentBuilderRangeProto();
        OneofSegmentBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentBuilderDomain expected = OneofTestObjectsCreator.createOneofSegmentBuilderRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorPrimitiveToProto() {
        OneofSegmentConstructorDomain domain = OneofTestObjectsCreator.createOneofSegmentConstructorPointDomain();
        OneofSegmentConstructorProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorProto expected = OneofTestObjectsCreator.createOneofSegmentConstructorPointProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorPrimitiveToDomain() {
        OneofSegmentConstructorProto proto = OneofTestObjectsCreator.createOneofSegmentConstructorPointProto();
        OneofSegmentConstructorDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorDomain expected = OneofTestObjectsCreator.createOneofSegmentConstructorPointDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorMessageToProto() {
        OneofSegmentConstructorDomain domain = OneofTestObjectsCreator.createOneofSegmentConstructorOpenRangeDomain();
        OneofSegmentConstructorProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorProto expected = OneofTestObjectsCreator.createOneofSegmentConstructorOpenRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorMessageToDomain() {
        OneofSegmentConstructorProto proto = OneofTestObjectsCreator.createOneofSegmentConstructorOpenRangeProto();
        OneofSegmentConstructorDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorDomain expected = OneofTestObjectsCreator.createOneofSegmentConstructorOpenRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorMapperToProto() {
        OneofSegmentConstructorDomain domain = OneofTestObjectsCreator.createOneofSegmentConstructorRangeDomain();
        OneofSegmentConstructorProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorProto expected = OneofTestObjectsCreator.createOneofSegmentConstructorRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorMapperToDomain() {
        OneofSegmentConstructorProto proto = OneofTestObjectsCreator.createOneofSegmentConstructorRangeProto();
        OneofSegmentConstructorDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorDomain expected = OneofTestObjectsCreator.createOneofSegmentConstructorRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorBuilderPrimitiveToProto() {
        OneofSegmentConstructorBuilderDomain domain = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderPointDomain();
        OneofSegmentConstructorBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorBuilderProto expected = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderPointProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorBuilderPrimitiveToDomain() {
        OneofSegmentConstructorBuilderProto proto = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderPointProto();
        OneofSegmentConstructorBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorBuilderDomain expected = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderPointDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorBuilderMessageToProto() {
        OneofSegmentConstructorBuilderDomain domain = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderOpenRangeDomain();
        OneofSegmentConstructorBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorBuilderProto expected = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderOpenRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorBuilderMessageToDomain() {
        OneofSegmentConstructorBuilderProto proto = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderOpenRangeProto();
        OneofSegmentConstructorBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorBuilderDomain expected = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderOpenRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassConstructorBuilderMapperToProto() {
        OneofSegmentConstructorBuilderDomain domain = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderRangeDomain();
        OneofSegmentConstructorBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentConstructorBuilderProto expected = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassConstructorBuilderMapperToDomain() {
        OneofSegmentConstructorBuilderProto proto = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderRangeProto();
        OneofSegmentConstructorBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentConstructorBuilderDomain expected = OneofTestObjectsCreator.createOneofSegmentConstructorBuilderRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnClassMessageToProto() {
        ShapeDomain domain = OneofTestObjectsCreator.createShapeDomain();
        ShapeProto proto = ProtoDomainConverter.toProto(domain);
        ShapeProto expected = OneofTestObjectsCreator.createShapeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnClassMessageToDomain() {
        ShapeProto proto = OneofTestObjectsCreator.createShapeProto();
        ShapeDomain domain = ProtoDomainConverter.toDomain(proto);
        ShapeDomain expected = OneofTestObjectsCreator.createShapeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldBeanPrimitiveToDomain() {
        OneofSegmentFieldProto proto = OneofTestObjectsCreator.createOneofSegmentFieldPointProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = OneofTestObjectsCreator.createOneofSegmentFieldPointDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldBeanPrimitiveToProto() {
        OneofSegmentFieldDomain domain = OneofTestObjectsCreator.createOneofSegmentFieldPointDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = OneofTestObjectsCreator.createOneofSegmentFieldPointProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnFieldConverterToDomain() {
        OneofSegmentFieldProto proto = OneofTestObjectsCreator.createOneofSegmentFieldRangeProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = OneofTestObjectsCreator.createOneofSegmentFieldRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldConverterToProto() {
        OneofSegmentFieldDomain domain = OneofTestObjectsCreator.createOneofSegmentFieldRangeDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = OneofTestObjectsCreator.createOneofSegmentFieldRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnFieldConstructorToDomain() {
        OneofSegmentFieldProto proto = OneofTestObjectsCreator.createOneofSegmentFieldInfinityStartProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = OneofTestObjectsCreator.createOneofSegmentFieldInfinityStartDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldConstructorToProto() {
        OneofSegmentFieldDomain domain = OneofTestObjectsCreator.createOneofSegmentFieldInfinityStartDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = OneofTestObjectsCreator.createOneofSegmentFieldInfinityStartProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnFieldMessageToDomain() {
        OneofSegmentFieldProto proto = OneofTestObjectsCreator.createOneofSegmentFieldEmptyRangeProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = OneofTestObjectsCreator.createOneofSegmentFieldEmptyRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldMessageToProto() {
        OneofSegmentFieldDomain domain = OneofTestObjectsCreator.createOneofSegmentFieldEmptyRangeDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = OneofTestObjectsCreator.createOneofSegmentFieldEmptyRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnFieldConstructorBuilderToDomain() {
        OneofSegmentFieldProto proto = OneofTestObjectsCreator.createOneofSegmentFieldInfinityEndProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = OneofTestObjectsCreator.createOneofSegmentFieldInfinityEndDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldConstructorBuilderToProto() {
        OneofSegmentFieldDomain domain = OneofTestObjectsCreator.createOneofSegmentFieldInfinityEndDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = OneofTestObjectsCreator.createOneofSegmentFieldInfinityEndProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOnFieldBuilderToDomain() {
        OneofSegmentFieldProto proto = OneofTestObjectsCreator.createOneofSegmentFieldOpenRangeProto();
        OneofSegmentFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofSegmentFieldDomain expected = OneofTestObjectsCreator.createOneofSegmentFieldOpenRangeDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofOnFieldBuilderToProto() {
        OneofSegmentFieldDomain domain = OneofTestObjectsCreator.createOneofSegmentFieldOpenRangeDomain();
        OneofSegmentFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofSegmentFieldProto expected = OneofTestObjectsCreator.createOneofSegmentFieldOpenRangeProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofDomainImplHierarchyToDomain() {
        OneofImplHierarchyProto proto = OneofTestObjectsCreator.createOneofImplHierarchyProto();
        OneofImplHierarchyDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofImplHierarchyDomain expected = OneofTestObjectsCreator.createOneofImplHierarchyDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofDomainImplHierarchyToProto() {
        OneofImplHierarchyDomain domain = OneofTestObjectsCreator.createOneofImplHierarchyDomain();
        OneofImplHierarchyProto proto = ProtoDomainConverter.toProto(domain);
        OneofImplHierarchyProto expected = OneofTestObjectsCreator.createOneofImplHierarchyProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofDomainImplHierarchySubclassToProto() {
        OneofImplHierarchyDomain domain = OneofTestObjectsCreator.createOneofImplHierarchyDomainSubclass();
        OneofImplHierarchyProto proto = ProtoDomainConverter.toProto(domain);
        OneofImplHierarchyProto expected = OneofTestObjectsCreator.createOneofImplHierarchyProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofDomainImplHierarchyFieldToDomain() {
        OneofImplHierarchyFieldProto proto = OneofTestObjectsCreator.createOneofImplHierarchyFieldProto();
        OneofImplHierarchyFieldDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofImplHierarchyFieldDomain expected = OneofTestObjectsCreator.createOneofImplHierarchyFieldDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofDomainImplHierarchyFieldToProto() {
        OneofImplHierarchyFieldDomain domain = OneofTestObjectsCreator.createOneofImplHierarchyFieldDomain();
        OneofImplHierarchyFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofImplHierarchyFieldProto expected = OneofTestObjectsCreator.createOneofImplHierarchyFieldProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofDomainImplHierarchyFieldSubclassToProto() {
        OneofImplHierarchyFieldDomain domain = OneofTestObjectsCreator.createOneofImplHierarchyFieldDomainSubclass();
        OneofImplHierarchyFieldProto proto = ProtoDomainConverter.toProto(domain);
        OneofImplHierarchyFieldProto expected = OneofTestObjectsCreator.createOneofImplHierarchyFieldProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOneImplSeveralFieldsToProto() {
        OneofOneImplSeveralFieldsDomain domain = OneofTestObjectsCreator.createOneofOneImplSeveralFieldsDomain();
        OneofOneImplSeveralFieldsProto proto = ProtoDomainConverter.toProto(domain);
        OneofOneImplSeveralFieldsProto expected = OneofTestObjectsCreator.createOneofOneImplSeveralFieldsProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofOneImplSeveralFieldsToDomain() {
        OneofOneImplSeveralFieldsProto proto = OneofTestObjectsCreator.createOneofOneImplSeveralFieldsProto();
        OneofOneImplSeveralFieldsDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofOneImplSeveralFieldsDomain expected = OneofTestObjectsCreator.createOneofOneImplSeveralFieldsDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofDefinedFromFieldAnnotationPojoToProto() {
        OneofDefinedFromFieldAnnotationPojoDomain domain = OneofTestObjectsCreator.createOneofDefinedFromFieldAnnotationPojoDomain();
        OneofDefinedFromFieldAnnotationPojoProto proto = ProtoDomainConverter.toProto(domain);
        OneofDefinedFromFieldAnnotationPojoProto expected = OneofTestObjectsCreator.createOneofDefinedFromFieldAnnotationPojoProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofDefinedFromFieldAnnotationPojoToDomain() {
        OneofDefinedFromFieldAnnotationPojoProto proto = OneofTestObjectsCreator.createOneofDefinedFromFieldAnnotationPojoProto();
        OneofDefinedFromFieldAnnotationPojoDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofDefinedFromFieldAnnotationPojoDomain expected = OneofTestObjectsCreator.createOneofDefinedFromFieldAnnotationPojoDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofDefinedFromFieldAnnotationBuilderToProto() {
        OneofDefinedFromFieldAnnotationBuilderDomain domain = OneofTestObjectsCreator.createOneofDefinedFromFieldAnnotationBuilderDomain();
        OneofDefinedFromFieldAnnotationBuilderProto proto = ProtoDomainConverter.toProto(domain);
        OneofDefinedFromFieldAnnotationBuilderProto expected = OneofTestObjectsCreator.createOneofDefinedFromFieldAnnotationBuilderProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofDefinedFromFieldAnnotationBuilderToDomain() {
        OneofDefinedFromFieldAnnotationBuilderProto proto = OneofTestObjectsCreator.createOneofDefinedFromFieldAnnotationBuilderProto();
        OneofDefinedFromFieldAnnotationBuilderDomain domain = ProtoDomainConverter.toDomain(proto);
        OneofDefinedFromFieldAnnotationBuilderDomain expected = OneofTestObjectsCreator.createOneofDefinedFromFieldAnnotationBuilderDomain();

        assertEquals(expected, domain);
    }
}
