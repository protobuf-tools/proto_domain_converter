package org.silbertb.proto.domainconverter.basic;

import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.basic.domain.*;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofWithFieldInheritance;
import org.silbertb.proto.domainconverter.test.proto.BytesProto;
import org.silbertb.proto.domainconverter.test.proto.StringProto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class BasicTest {

    @Test
    void testPrimitivesToProto() {
        PrimitiveDomain domain = BasicTestObjectsCreator.createPrimitiveDomain();
        org.silbertb.proto.domainconverter.test.proto.Primitives proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.Primitives expected = BasicTestObjectsCreator.createPrimitivesProto();

        assertEquals(expected, proto);
    }

    @Test
    void testPrimitivesToDomain() {
        org.silbertb.proto.domainconverter.test.proto.Primitives proto = BasicTestObjectsCreator.createPrimitivesProto();
        PrimitiveDomain domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveDomain expected = BasicTestObjectsCreator.createPrimitiveDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testPojoUpdateDomain() {
        org.silbertb.proto.domainconverter.test.proto.Primitives proto = BasicTestObjectsCreator.createPrimitivesProto();
        PrimitiveDomain domain = new PrimitiveDomain();

        PrimitiveDomain domainResult = ProtoDomainConverter.toDomain(proto, domain);
        assertSame(domain, domainResult);

        PrimitiveDomain expected = BasicTestObjectsCreator.createPrimitiveDomain();
        assertEquals(expected, domain);
    }

    @Test
    void testEmptyPrimitivesToProto() {
        PrimitiveDomain domain = new PrimitiveDomain();
        org.silbertb.proto.domainconverter.test.proto.Primitives proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.Primitives expected =
                org.silbertb.proto.domainconverter.test.proto.Primitives.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testEmptyPrimitivesToDomain() {
        org.silbertb.proto.domainconverter.test.proto.Primitives proto =
                org.silbertb.proto.domainconverter.test.proto.Primitives.newBuilder().build();
        PrimitiveDomain domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveDomain expected = new PrimitiveDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testStringToProto() {
        StringDomain domain = BasicTestObjectsCreator.createStringDomain();
        StringProto proto = ProtoDomainConverter.toProto(domain);
        StringProto expected = BasicTestObjectsCreator.createStringProto();

        assertEquals(expected, proto);
    }

    @Test
    void testStringToDomain() {
        StringProto proto = BasicTestObjectsCreator.createStringProto();
        StringDomain domain = ProtoDomainConverter.toDomain(proto);
        StringDomain expected = BasicTestObjectsCreator.createStringDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyStringToProto() {
        StringDomain domain = new StringDomain();
        StringProto proto = ProtoDomainConverter.toProto(domain);
        StringProto expected = StringProto.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testBytesToProto() {
        BytesDomain domain = BasicTestObjectsCreator.createBytesDomain();
        BytesProto proto = ProtoDomainConverter.toProto(domain);
        BytesProto expected = BasicTestObjectsCreator.createBytesProto();

        assertEquals(expected, proto);
    }

    @Test
    void testBytesToDomain() {
        BytesProto proto = BasicTestObjectsCreator.createBytesProto();
        BytesDomain domain = ProtoDomainConverter.toDomain(proto);
        BytesDomain expected = BasicTestObjectsCreator.createBytesDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyBytesToProto() {
        BytesDomain domain = new BytesDomain();
        BytesProto proto = ProtoDomainConverter.toProto(domain);
        BytesProto expected = BytesProto.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testEmptyStringToDomain() {
        StringProto proto = StringProto.newBuilder().build();
        StringDomain domain = ProtoDomainConverter.toDomain(proto);
        StringDomain expected = new StringDomain();
        expected.setStringValue("");

        assertEquals(expected, domain);
    }

    @Test
    void testSimpleContainerToProto() {
        SimpleContainer domain = BasicTestObjectsCreator.createSimpleContainerDomain();
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer expected = BasicTestObjectsCreator.createSimpleContainerProto();

        assertEquals(expected, proto);
    }

    @Test
    void testSimpleContainerToDomain() {
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer proto = BasicTestObjectsCreator.createSimpleContainerProto();
        SimpleContainer domain = ProtoDomainConverter.toDomain(proto);
        SimpleContainer expected = BasicTestObjectsCreator.createSimpleContainerDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptySimpleContainerToProto() {
        SimpleContainer domain = new SimpleContainer();
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer expected =
                org.silbertb.proto.domainconverter.test.proto.SimpleContainer.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testEmptySimpleContainerToDomain() {
        org.silbertb.proto.domainconverter.test.proto.SimpleContainer proto =
                org.silbertb.proto.domainconverter.test.proto.SimpleContainer.newBuilder().build();
        SimpleContainer domain = ProtoDomainConverter.toDomain(proto);
        SimpleContainer expected = new SimpleContainer();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofWithoutInheritanceToProto() {
        OneofWithoutInheritance domain = BasicTestObjectsCreator.createOneofWithoutInheritanceDomain();
        org.silbertb.proto.domainconverter.test.proto.OneofWithoutInheritance proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.OneofWithoutInheritance expected = BasicTestObjectsCreator.createOneofWithoutInheritanceProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofWithoutInheritanceToDomain() {
        org.silbertb.proto.domainconverter.test.proto.OneofWithoutInheritance proto = BasicTestObjectsCreator.createOneofWithoutInheritanceProto();
        OneofWithoutInheritance domain = ProtoDomainConverter.toDomain(proto);
        OneofWithoutInheritance expected = BasicTestObjectsCreator.createOneofWithoutInheritanceDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testOneofWithInheritanceToProto() {
        OneofWithFieldInheritance domain = BasicTestObjectsCreator.createOneofWithInheritanceDomain();
        org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance proto = ProtoDomainConverter.toProto(domain);
        org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance expected = BasicTestObjectsCreator.createOneofWithFieldInheritanceProto();

        assertEquals(expected, proto);
    }

    @Test
    void testOneofWithInheritanceToDomain() {
        org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance proto = BasicTestObjectsCreator.createOneofWithFieldInheritanceProto();
        OneofWithFieldInheritance domain = ProtoDomainConverter.toDomain(proto);
        OneofWithFieldInheritance expected = BasicTestObjectsCreator.createOneofWithInheritanceDomain();

        assertEquals(expected, domain);
    }
}
