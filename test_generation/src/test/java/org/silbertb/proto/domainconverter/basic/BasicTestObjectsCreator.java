package org.silbertb.proto.domainconverter.basic;

import com.google.protobuf.ByteString;
import org.silbertb.proto.domainconverter.basic.domain.*;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofIntImplDomain;
import org.silbertb.proto.domainconverter.oneof.domain.field.OneofWithFieldInheritance;
import org.silbertb.proto.domainconverter.test.proto.BytesProto;
import org.silbertb.proto.domainconverter.test.proto.StringProto;

public class BasicTestObjectsCreator {
    public static BytesDomain createBytesDomain() {
        BytesDomain domain = new BytesDomain();
        domain.setBytesValue(new byte[]{0x1b, 0x2b});
        return domain;
    }

    public static BytesProto createBytesProto() {
        return BytesProto.newBuilder().setBytesValue(ByteString.copyFrom(new byte[]{0x1b, 0x2b})).build();
    }

    public static OneofWithFieldInheritance createOneofWithInheritanceDomain() {
        OneofWithFieldInheritance domain = new org.silbertb.proto.domainconverter.oneof.domain.field.OneofWithFieldInheritance();
        OneofIntImplDomain oneofIntImplDomain = new OneofIntImplDomain();
        oneofIntImplDomain.setIntVal(3);
        domain.setValue(oneofIntImplDomain);
        return domain;
    }

    public static org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance createOneofWithFieldInheritanceProto() {
        return org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance.newBuilder()
                .setIntVal(3)
                .build();
    }

    public static OneofWithoutInheritance createOneofWithoutInheritanceDomain() {
        OneofWithoutInheritance domain = new OneofWithoutInheritance();
        domain.setIntVal(3);
        return domain;
    }

    public static org.silbertb.proto.domainconverter.test.proto.OneofWithoutInheritance createOneofWithoutInheritanceProto() {
        return org.silbertb.proto.domainconverter.test.proto.OneofWithoutInheritance.newBuilder()
                .setIntVal(3)
                .build();
    }

    public static PrimitiveDomain createPrimitiveDomain() {
        PrimitiveDomain primitiveDomain = new PrimitiveDomain();
        primitiveDomain.setBooleanValue(true);
        primitiveDomain.setFloatValue(-0.1f);
        primitiveDomain.setDoubleValue(-0.5);
        primitiveDomain.setIntValue(-1);
        primitiveDomain.setLongValue(-2L);

        return primitiveDomain;
    }

    public static org.silbertb.proto.domainconverter.test.proto.Primitives createPrimitivesProto() {
        return org.silbertb.proto.domainconverter.test.proto.Primitives.newBuilder()
                .setBooleanValue(true)
                .setFloatValue(-0.1f)
                .setDoubleValue(-0.5)
                .setIntValue(-1)
                .setLongValue(-2L)
                .build();
    }

    public static StringDomain createStringDomain() {
        StringDomain stringDomain = new StringDomain();
        stringDomain.setStringValue("aaaa");
        return stringDomain;
    }

    public static StringProto createStringProto() {
        return StringProto.newBuilder().setStringValue("aaaa").build();
    }

    public static SimpleContainer createSimpleContainerDomain() {
        SimpleContainer simpleContainerDomain = new SimpleContainer();
        simpleContainerDomain.setPrimitives(createPrimitiveDomain());

        return simpleContainerDomain;
    }

    public static org.silbertb.proto.domainconverter.test.proto.SimpleContainer createSimpleContainerProto() {
        return org.silbertb.proto.domainconverter.test.proto.SimpleContainer.newBuilder()
                .setPrimitives(createPrimitivesProto())
                .build();
    }
}
