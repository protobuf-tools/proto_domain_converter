package org.silbertb.proto.domainconverter.oneof;

import org.silbertb.proto.domainconverter.oneof.domain.RangeDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.bean.OneofSegmentOpenRange;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.bean.OneofSegmentPoint;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.bean.OneofSegmentRange;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.class_builder.OneofSegmentBuilderOpenRange;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.class_builder.OneofSegmentBuilderPoint;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.class_builder.OneofSegmentBuilderRange;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.constructor.OneofSegmentConstructorOpenRange;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.constructor.OneofSegmentConstructorPoint;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.constructor.OneofSegmentConstructorRange;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.constructor_builder.OneofSegmentConstructorBuilderOpenRange;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.constructor_builder.OneofSegmentConstructorBuilderPoint;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.constructor_builder.OneofSegmentConstructorBuilderRange;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.no_members.CircleDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_class.no_members.ShapeDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_field.*;
import org.silbertb.proto.domainconverter.oneof.domain.domain_impl_hierarchy.domain_class.OneofImplHierarchyDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_impl_hierarchy.domain_class.OneofImplHierarchyImpl2;
import org.silbertb.proto.domainconverter.oneof.domain.domain_impl_hierarchy.domain_field.OneofImplHierarchyFieldDomain;
import org.silbertb.proto.domainconverter.oneof.domain.domain_impl_hierarchy.domain_field.OneofImplHierarchyFieldImpl2;
import org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain.*;
import org.silbertb.proto.domainconverter.test.proto.oneof.*;

public class OneofTestObjectsCreator {
    public static OneofSegmentPoint createOneofSegmentPointDomain() {
        OneofSegmentPoint oneofSegmentPoint = new OneofSegmentPoint();
        oneofSegmentPoint.setValue("1");
        oneofSegmentPoint.setName("Point");

        return oneofSegmentPoint;
    }

    public static OneofSegmentProto createOneofSegmentPointProto() {
        return OneofSegmentProto.newBuilder()
                .setPoint(1)
                .setName("Point")
                .build();
    }

    public static OneofSegmentOpenRange createOneofSegmentOpenRangeDomain() {
        OneofSegmentOpenRange oneofSegmentOpenRange = new OneofSegmentOpenRange();
        oneofSegmentOpenRange.setRange(RangeDomain.builder().start(1).end(3).build());
        oneofSegmentOpenRange.setName("Open Range");

        return oneofSegmentOpenRange;
    }

    public static OneofSegmentProto createOneofSegmentOpenRangeProto() {
        return OneofSegmentProto.newBuilder()
                .setOpenRange(RangeProto.newBuilder().setStart(1).setEnd(3))
                .setName("Open Range")
                .build();
    }

    public static OneofSegmentRange createOneofSegmentRangeDomain() {
        OneofSegmentRange oneofSegmentRange = new OneofSegmentRange(1, 3);
        oneofSegmentRange.setName("Range");

        return oneofSegmentRange;
    }

    public static OneofSegmentProto createOneofSegmentRangeProto() {
        return OneofSegmentProto.newBuilder()
                .setRange("1-3")
                .setName("Range")
                .build();
    }

    public static OneofSegmentBuilderPoint createOneofSegmentBuilderPointDomain() {
        return OneofSegmentBuilderPoint.builder()
                .name("Point")
                .value("1")
                .build();
    }

    public static OneofSegmentBuilderProto createOneofSegmentBuilderPointProto() {
        return OneofSegmentBuilderProto.newBuilder()
                .setPoint(1)
                .setName("Point")
                .build();
    }

    public static OneofSegmentBuilderOpenRange createOneofSegmentBuilderOpenRangeDomain() {
        return OneofSegmentBuilderOpenRange.builder()
                .range(RangeDomain.builder().start(1).end(3).build())
                .name("Open Range")
                .build();
    }

    public static OneofSegmentBuilderProto createOneofSegmentBuilderOpenRangeProto() {
        return OneofSegmentBuilderProto.newBuilder()
                .setOpenRange(RangeProto.newBuilder().setStart(1).setEnd(3))
                .setName("Open Range")
                .build();
    }

    public static OneofSegmentBuilderRange createOneofSegmentBuilderRangeDomain() {
        return OneofSegmentBuilderRange.builder()
                .start(1)
                .end(3)
                .name("Range")
                .build();
    }

    public static OneofSegmentBuilderProto createOneofSegmentBuilderRangeProto() {
        return OneofSegmentBuilderProto.newBuilder()
                .setRange("1-3")
                .setName("Range")
                .build();
    }

    public static OneofSegmentConstructorPoint createOneofSegmentConstructorPointDomain() {
        return new OneofSegmentConstructorPoint("Point", "1");
    }

    public static OneofSegmentConstructorProto createOneofSegmentConstructorPointProto() {
        return OneofSegmentConstructorProto.newBuilder()
                .setPoint(1)
                .setName("Point")
                .build();
    }

    public static OneofSegmentConstructorOpenRange createOneofSegmentConstructorOpenRangeDomain() {
        return new OneofSegmentConstructorOpenRange("Open Range",
                RangeDomain.builder().start(1).end(3).build());
    }

    public static OneofSegmentConstructorProto createOneofSegmentConstructorOpenRangeProto() {
        return OneofSegmentConstructorProto.newBuilder()
                .setOpenRange(RangeProto.newBuilder().setStart(1).setEnd(3))
                .setName("Open Range")
                .build();
    }

    public static OneofSegmentConstructorRange createOneofSegmentConstructorRangeDomain() {
        return new OneofSegmentConstructorRange("Range", 1, 3);
    }

    public static OneofSegmentConstructorProto createOneofSegmentConstructorRangeProto() {
        return OneofSegmentConstructorProto.newBuilder()
                .setRange("1-3")
                .setName("Range")
                .build();
    }

    public static OneofSegmentConstructorBuilderPoint createOneofSegmentConstructorBuilderPointDomain() {
        return OneofSegmentConstructorBuilderPoint.builder().name("Point").value("1").build();
    }

    public static OneofSegmentConstructorBuilderProto createOneofSegmentConstructorBuilderPointProto() {
        return OneofSegmentConstructorBuilderProto.newBuilder()
                .setPoint(1)
                .setName("Point")
                .build();
    }

    public static OneofSegmentConstructorBuilderOpenRange createOneofSegmentConstructorBuilderOpenRangeDomain() {
        return OneofSegmentConstructorBuilderOpenRange.builder()
                .name("Open Range")
                .range(RangeDomain.builder().start(1).end(3).build())
                .build();
    }

    public static OneofSegmentConstructorBuilderProto createOneofSegmentConstructorBuilderOpenRangeProto() {
        return OneofSegmentConstructorBuilderProto.newBuilder()
                .setOpenRange(RangeProto.newBuilder().setStart(1).setEnd(3))
                .setName("Open Range")
                .build();
    }

    public static OneofSegmentConstructorBuilderRange createOneofSegmentConstructorBuilderRangeDomain() {
        return OneofSegmentConstructorBuilderRange.builder().name("Range").start(1).end(3).build();
    }

    public static OneofSegmentConstructorBuilderProto createOneofSegmentConstructorBuilderRangeProto() {
        return OneofSegmentConstructorBuilderProto.newBuilder()
                .setRange("1-3")
                .setName("Range")
                .build();
    }

    public static OneofSegmentFieldDomain createOneofSegmentFieldPointDomain() {
        OneofSegmentFieldPoint oneofSegmentFieldPoint = new OneofSegmentFieldPoint();
        oneofSegmentFieldPoint.setValue("1");

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Point");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldPoint);
        return oneofSegmentFieldDomain;
    }

    public static OneofSegmentFieldProto createOneofSegmentFieldPointProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setPoint(1)
                .setName("Point")
                .build();
    }

    public static OneofSegmentFieldDomain createOneofSegmentFieldRangeDomain() {
        OneofSegmentFieldRange oneofSegmentFieldRange = new OneofSegmentFieldRange(1, 3);

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Range");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldRange);
        return oneofSegmentFieldDomain;
    }

    public static OneofSegmentFieldProto createOneofSegmentFieldRangeProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setRange("1-3")
                .setName("Range")
                .build();
    }

    public static OneofSegmentFieldDomain createOneofSegmentFieldInfinityStartDomain() {
        OneofInfinityStartSegmentField oneofSegmentFieldRange = new OneofInfinityStartSegmentField(1);

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Infinity Start");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldRange);
        return oneofSegmentFieldDomain;
    }

    public static OneofSegmentFieldProto createOneofSegmentFieldInfinityStartProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setInfinityStart(1)
                .setName("Infinity Start")
                .build();
    }

    public static OneofSegmentFieldDomain createOneofSegmentFieldEmptyRangeDomain() {
        OneofSegmentFieldEmptyRange oneofSegmentFieldEmptyRange = new OneofSegmentFieldEmptyRange();

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Empty Range");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldEmptyRange);
        return oneofSegmentFieldDomain;
    }

    public static OneofSegmentFieldProto createOneofSegmentFieldEmptyRangeProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setEmptyRange(EmptyRangeProto.newBuilder().build())
                .setName("Empty Range")
                .build();
    }

    public static OneofSegmentFieldDomain createOneofSegmentFieldInfinityEndDomain() {
        OneofInfinityEndSegmentField oneofSegmentFieldRange = new OneofInfinityEndSegmentField(1);

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Infinity End");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldRange);
        return oneofSegmentFieldDomain;
    }

    public static OneofSegmentFieldProto createOneofSegmentFieldInfinityEndProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setInfinityEnd(1)
                .setName("Infinity End")
                .build();
    }

    public static OneofSegmentFieldDomain createOneofSegmentFieldOpenRangeDomain() {
        OneofSegmentFieldOpenRange oneofSegmentFieldOpenRange = OneofSegmentFieldOpenRange
                .builder()
                .range(RangeDomain.builder().start(1).end(3).build())
                .build();

        OneofSegmentFieldDomain oneofSegmentFieldDomain = new OneofSegmentFieldDomain();
        oneofSegmentFieldDomain.setName("Open Range");
        oneofSegmentFieldDomain.setSegment(oneofSegmentFieldOpenRange);
        return oneofSegmentFieldDomain;
    }

    public static OneofSegmentFieldProto createOneofSegmentFieldOpenRangeProto() {
        return OneofSegmentFieldProto.newBuilder()
                .setOpenRange(RangeProto.newBuilder().setStart(1).setEnd(3))
                .setName("Open Range")
                .build();
    }

    public static ShapeDomain createShapeDomain() {
        return new CircleDomain(3);
    }

    public static ShapeProto createShapeProto() {
        return ShapeProto.newBuilder()
                .setCircle(CircleProto.newBuilder().setRadius(3))
                .build();
    }

    public static OneofImplHierarchyDomain createOneofImplHierarchyDomain() {
        return new OneofImplHierarchyImpl2("aaa");
    }

    public static OneofImplHierarchyDomain createOneofImplHierarchyDomainSubclass() {
        return new OneofImplHierarchyImpl2("aaa") {
        };
    }

    public static OneofImplHierarchyProto createOneofImplHierarchyProto() {
        return OneofImplHierarchyProto.newBuilder().setVal2("aaa").build();
    }

    public static OneofImplHierarchyFieldDomain createOneofImplHierarchyFieldDomain() {
        OneofImplHierarchyFieldDomain oneofImplHierarchyFieldDomain = new OneofImplHierarchyFieldDomain();
        oneofImplHierarchyFieldDomain.setValue(new OneofImplHierarchyFieldImpl2("aaa"));
        return oneofImplHierarchyFieldDomain;
    }

    public static OneofImplHierarchyFieldProto createOneofImplHierarchyFieldProto() {
        return OneofImplHierarchyFieldProto.newBuilder().setVal2("aaa").build();
    }

    public static OneofImplHierarchyFieldDomain createOneofImplHierarchyFieldDomainSubclass() {
        OneofImplHierarchyFieldDomain oneofImplHierarchyFieldDomain = new OneofImplHierarchyFieldDomain();
        oneofImplHierarchyFieldDomain.setValue(new OneofImplHierarchyFieldImpl2("aaa") {
        });
        return oneofImplHierarchyFieldDomain;
    }

    public static OneofOneImplSeveralFieldsProto createOneofOneImplSeveralFieldsProto() {
        return OneofOneImplSeveralFieldsProto.newBuilder()
                .setIntVal(1)
                .setDoubleVal(2.0)
                .setStrVal("abc")
                .build();
    }

    public static OneofOneImplSeveralFieldsDomain createOneofOneImplSeveralFieldsDomain() {
        OneofOneImplSeveralFieldsDomain domain = new OneofOneImplSeveralFieldsDomain();
        domain.setVal1(new StringWrapperConstructor("1"));
        domain.setVal2(new StringWrapperConstructor("2.0"));
        domain.setVal3(new StringWrapperConstructor("abc"));

        return domain;
    }

    public static OneofDefinedFromFieldAnnotationPojoDomain createOneofDefinedFromFieldAnnotationPojoDomain() {
        StringWrapperPojo stringWrapperPojo = new StringWrapperPojo();
        stringWrapperPojo.setStrVal("10");
        return OneofDefinedFromFieldAnnotationPojoDomain.builder()
                .value(stringWrapperPojo)
                .build();
    }

    public static OneofDefinedFromFieldAnnotationPojoProto createOneofDefinedFromFieldAnnotationPojoProto() {
        return OneofDefinedFromFieldAnnotationPojoProto.newBuilder()
                .setIntVal(10)
                .build();
    }

    public static OneofDefinedFromFieldAnnotationBuilderDomain createOneofDefinedFromFieldAnnotationBuilderDomain() {
        return OneofDefinedFromFieldAnnotationBuilderDomain.builder()
                .value(StringWrapperBuilder.builder().strVal("10").build())
                .build();
    }

    public static OneofDefinedFromFieldAnnotationBuilderProto createOneofDefinedFromFieldAnnotationBuilderProto() {
        return OneofDefinedFromFieldAnnotationBuilderProto.newBuilder()
                .setIntVal(10)
                .build();
    }

}
