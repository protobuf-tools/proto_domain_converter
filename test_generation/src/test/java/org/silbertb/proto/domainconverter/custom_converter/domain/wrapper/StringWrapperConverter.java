package org.silbertb.proto.domainconverter.custom_converter.domain.wrapper;

import org.silbertb.proto.domainconverter.custom.TypeConverter;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.StringWrapper;

public class StringWrapperConverter implements TypeConverter<String, StringWrapper> {
    @Override
    public String toDomainValue(StringWrapper protoValue) {
        return protoValue.getValue();
    }

    @Override
    public boolean shouldAssignToProto(String domainValue) {
        return domainValue != null;
    }

    @Override
    public StringWrapper toProtobufValue(String domainValue) {
        return StringWrapper.newBuilder().setValue(domainValue).build();
    }
}