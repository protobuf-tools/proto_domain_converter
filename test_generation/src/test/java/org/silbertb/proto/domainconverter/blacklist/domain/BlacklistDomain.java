package org.silbertb.proto.domainconverter.blacklist.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.annotations.ProtoIgnore;
import org.silbertb.proto.domainconverter.custom_converter.domain.IntStrConverter;
import org.silbertb.proto.domainconverter.test.proto.blacklist.BlacklistProto;

@Data
@ProtoClass(protoClass = BlacklistProto.class, blacklist = true)
public class BlacklistDomain {
    public static String staticFieldsShouldBeIgnored = "";
    private double doubleValue;

    @ProtoField(protoName = "long_value")
    private long longVal;

    @ProtoConverter(converter = IntStrConverter.class)
    private String intValue;

    @EqualsAndHashCode.Exclude
    @ProtoIgnore
    private String anotherField;

}
