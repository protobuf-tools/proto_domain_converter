package org.silbertb.proto.domainconverter.oneof.domain.converter.onclass;

import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;

@ProtoConverter(converter = RangeConverter.class)
@Value
public class Range implements SegmentDomain {
    int start;
    int end;
}
