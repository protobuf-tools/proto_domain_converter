package org.silbertb.proto.domainconverter.oneof.domain.converter.onclass;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@Data
public class Point implements SegmentDomain {

    @ProtoField(protoName = "point")
    private int value;
}
