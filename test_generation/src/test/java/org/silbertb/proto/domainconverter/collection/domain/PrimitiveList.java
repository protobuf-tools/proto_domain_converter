package org.silbertb.proto.domainconverter.collection.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;
import org.silbertb.proto.domainconverter.test.proto.collection.PrimitiveListProto;

import java.util.List;

@Data
@ProtoClass(protoClass = PrimitiveListProto.class)
public class PrimitiveList {

    @ProtoField
    private List<Integer> intList;
}
