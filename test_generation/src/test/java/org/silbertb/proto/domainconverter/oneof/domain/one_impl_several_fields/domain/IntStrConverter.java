package org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain;

import org.silbertb.proto.domainconverter.custom.TypeConverter;

public class IntStrConverter implements TypeConverter<String, Integer> {

    @Override
    public String toDomainValue(Integer protoValue) {
        return protoValue.toString();
    }

    @Override
    public boolean shouldAssignToProto(String domainValue) {
        return true;
    }

    @Override
    public Integer toProtobufValue(String domainValue) {
        return Integer.valueOf(domainValue);
    }
}
