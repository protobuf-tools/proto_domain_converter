package org.silbertb.proto.domainconverter.global_mapper.domain;

import lombok.Builder;
import lombok.Value;
import org.silbertb.proto.domainconverter.annotations.ProtoBuilder;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.test.proto.GlobalMappersContainerProto;

import java.net.Inet4Address;
import java.time.Instant;

@Builder
@Value
@ProtoBuilder
@ProtoClass(protoClass = GlobalMappersContainerProto.class, blacklist = true)
public class GlobalMappersContainerDomain {
    Instant timestamp;
    Inet4Address ip;
}
