package org.silbertb.proto.domainconverter.global_mapper.domain;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.test.proto.IpAddressProto;

@ProtoClass(protoClass = IpAddressProto.class)
@Data
public class IpAddressDomain {

    @ProtoField
    @ProtoConverter(converter = StringBytesIpConverter.class)
    private String address;
}
