package org.silbertb.proto.domainconverter.collection.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;
import org.silbertb.proto.domainconverter.basic.domain.PrimitiveDomain;
import org.silbertb.proto.domainconverter.test.proto.collection.MapToMessageProto;

import java.util.Map;

@Data
@ProtoClass(protoClass = MapToMessageProto.class)
public class MapToMessage {

    @ProtoField
    Map<String, PrimitiveDomain> mapToMessage;
}
