package org.silbertb.proto.domainconverter.oneof.domain.field;

import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.basic.domain.PrimitiveDomain;
import lombok.Data;

@Data
@ProtoClass(protoClass = org.silbertb.proto.domainconverter.test.proto.OneofWithFieldInheritance.class)
public class OneofWithFieldInheritance {

    @OneofBase(oneOfFields = {
            @OneofField(protoField = "int_val", domainClass = OneofIntImplDomain.class, domainField = "intVal"),
            @OneofField(protoField = "primitives", domainClass = PrimitiveDomain.class)
    })
    private OneofBaseFieldDomain value;
}
