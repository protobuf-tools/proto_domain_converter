package org.silbertb.proto.domainconverter.custom_converter;

import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.custom_converter.domain.*;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomConverterTest {
    @Test
    void testCustomConverterToDomain() {
        CustomConverterProto proto = CustomConverterTestObjectsCreator.createCustomConverterProto();
        CustomConverter domain = ProtoDomainConverter.toDomain(proto);
        CustomConverter expected = CustomConverterTestObjectsCreator.createCustomConverterDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomConverterToProto() {
        CustomConverter domain = CustomConverterTestObjectsCreator.createCustomConverterDomain();
        CustomConverterProto proto = ProtoDomainConverter.toProto(domain);
        CustomConverterProto expected = CustomConverterTestObjectsCreator.createCustomConverterProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomListConverterToDomain() {
        CustomListConverterProto proto = CustomConverterTestObjectsCreator.createCustomListConverterProto();
        CustomListConverter domain = ProtoDomainConverter.toDomain(proto);
        CustomListConverter expected = CustomConverterTestObjectsCreator.createCustomListConverterDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomListConverterToProto() {
        CustomListConverter domain = CustomConverterTestObjectsCreator.createCustomListConverterDomain();
        CustomListConverterProto proto = ProtoDomainConverter.toProto(domain);
        CustomListConverterProto expected = CustomConverterTestObjectsCreator.createCustomListConverterProto();

        assertEquals(expected, proto);
    }

    @Test
    void testCustomMapConverterToDomain() {
        CustomMapConverterProto proto = CustomConverterTestObjectsCreator.createCustomMapConverterProto();
        CustomMapConverter domain = ProtoDomainConverter.toDomain(proto);
        CustomMapConverter expected = CustomConverterTestObjectsCreator.createCustomMapConverterDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testCustomMapConverterToProto() {
        CustomMapConverter domain = CustomConverterTestObjectsCreator.createCustomMapConverterDomain();
        CustomMapConverterProto proto = ProtoDomainConverter.toProto(domain);
        CustomMapConverterProto expected = CustomConverterTestObjectsCreator.createCustomMapConverterProto();

        assertEquals(expected, proto);
    }

    @Test
    void testMultiCustomConvertersToDomain() {
        MultiCustomConvertersProto proto = CustomConverterTestObjectsCreator.createMultiCustomConvertersProto();
        MultiCustomConvertersDomain domain = ProtoDomainConverter.toDomain(proto);
        MultiCustomConvertersDomain expected = CustomConverterTestObjectsCreator.createMultiCustomConvertersDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMultiCustomConvertersToProto() {
        MultiCustomConvertersDomain domain = CustomConverterTestObjectsCreator.createMultiCustomConvertersDomain();
        MultiCustomConvertersProto proto = ProtoDomainConverter.toProto(domain);
        MultiCustomConvertersProto expected = CustomConverterTestObjectsCreator.createMultiCustomConvertersProto();

        assertEquals(expected, proto);
    }

    @Test
    void testNullableProtoConverterToDomain() {
        NullableFieldsExampleProto proto = CustomConverterTestObjectsCreator.createNullableFieldsExampleProto();
        NullableFieldsExampleDomain domain = ProtoDomainConverter.toDomain(proto);
        NullableFieldsExampleDomain expected = CustomConverterTestObjectsCreator.createNullableFieldsExampleDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testNullableProtoConverterToProto() {
        NullableFieldsExampleDomain domain = CustomConverterTestObjectsCreator.createNullableFieldsExampleDomain();
        NullableFieldsExampleProto proto = ProtoDomainConverter.toProto(domain);
        NullableFieldsExampleProto expected = CustomConverterTestObjectsCreator.createNullableFieldsExampleProto();

        assertEquals(expected, proto);
    }
}
