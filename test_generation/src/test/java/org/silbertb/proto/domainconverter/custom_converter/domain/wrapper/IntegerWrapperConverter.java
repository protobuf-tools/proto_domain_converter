package org.silbertb.proto.domainconverter.custom_converter.domain.wrapper;

import org.silbertb.proto.domainconverter.custom.TypeConverter;
import org.silbertb.proto.domainconverter.test.proto.custom_converter.IntegerWrapper;

public class IntegerWrapperConverter implements TypeConverter<Integer, IntegerWrapper> {
    @Override
    public Integer toDomainValue(IntegerWrapper protoValue) {
        return protoValue.getValue();
    }

    @Override
    public boolean shouldAssignToProto(Integer domainValue) {
        return domainValue != null;
    }

    @Override
    public IntegerWrapper toProtobufValue(Integer domainValue) {
        return IntegerWrapper.newBuilder().setValue(domainValue).build();
    }
}
