package org.silbertb.proto.domainconverter.collection.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;
import org.silbertb.proto.domainconverter.basic.domain.PrimitiveDomain;
import org.silbertb.proto.domainconverter.test.proto.collection.MessageListProto;

import java.util.List;

@Data
@ProtoClass(protoClass = MessageListProto.class)
public class MessageListDomain {

    @ProtoField
    private List<PrimitiveDomain> messageList;
}
