package org.silbertb.proto.domainconverter.collection.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;
import org.silbertb.proto.domainconverter.test.proto.collection.ConcretePrimitiveListProto;

import java.util.LinkedList;

@Data
@ProtoClass(protoClass = ConcretePrimitiveListProto.class)
public class ConcretePrimitiveList {

    @ProtoField
    private LinkedList<Integer> intList;
}
