package org.silbertb.proto.domainconverter.collection;

import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.collection.domain.*;
import org.silbertb.proto.domainconverter.collection.domain.ConcreteMapToMessage;
import org.silbertb.proto.domainconverter.collection.domain.ConcretePrimitiveMap;
import org.silbertb.proto.domainconverter.collection.domain.MapToMessage;
import org.silbertb.proto.domainconverter.collection.domain.StringListDomain;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.collection.*;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CollectionTest {
    @Test
    void testConcretePrimitiveMapToDomain() {
        ConcretePrimitiveMapProto proto = CollectionTestObjecsCreator.createConcretePrimitiveMapProto();
        ConcretePrimitiveMap domain = ProtoDomainConverter.toDomain(proto);
        ConcretePrimitiveMap expected = CollectionTestObjecsCreator.createConcretePrimitiveMapDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMapToMessageToProto() {
        MapToMessage domain = CollectionTestObjecsCreator.createMapToMessageDomain();
        MapToMessageProto proto = ProtoDomainConverter.toProto(domain);
        MapToMessageProto expected = CollectionTestObjecsCreator.createMapToMessageProto();

        assertEquals(expected, proto);
    }

    @Test
    void testConcreteMapToMessageToDomain() {
        ConcreteMapToMessageProto proto = CollectionTestObjecsCreator.createConcreteMapToMessageProto();
        ConcreteMapToMessage domain = ProtoDomainConverter.toDomain(proto);
        ConcreteMapToMessage expected = CollectionTestObjecsCreator.createConcreteMapToMessageDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testPrimitiveListToProto() {
        PrimitiveList domain = CollectionTestObjecsCreator.createPrimitiveListDomain();
        PrimitiveListProto proto = ProtoDomainConverter.toProto(domain);
        PrimitiveListProto expected = CollectionTestObjecsCreator.createPrimitiveListProto();

        assertEquals(expected, proto);
    }

    @Test
    void testPrimitiveListToDomain() {
        PrimitiveListProto proto = CollectionTestObjecsCreator.createPrimitiveListProto();
        PrimitiveList domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveList expected = CollectionTestObjecsCreator.createPrimitiveListDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyPrimitiveListToProto() {
        PrimitiveList domain = new PrimitiveList();
        PrimitiveListProto proto = ProtoDomainConverter.toProto(domain);
        PrimitiveListProto expected =
                PrimitiveListProto.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testEmptyPrimitiveListToDomain() {
        PrimitiveListProto proto =
                PrimitiveListProto.newBuilder().build();
        PrimitiveList domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveList expected = new PrimitiveList();
        expected.setIntList(Collections.emptyList());

        assertEquals(expected, domain);
    }

    @Test
    void testPrimitiveConcreteListToDomain() {
        ConcretePrimitiveListProto proto = CollectionTestObjecsCreator.createConcretePrimitiveListProto();
        ConcretePrimitiveList domain = ProtoDomainConverter.toDomain(proto);
        ConcretePrimitiveList expected = CollectionTestObjecsCreator.createConcretePrimitiveListDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testStringListToProto() {
        StringListDomain domain = CollectionTestObjecsCreator.createStringListDomain();
        StringListProto proto = ProtoDomainConverter.toProto(domain);
        StringListProto expected = CollectionTestObjecsCreator.createStringListProto();

        assertEquals(expected, proto);
    }

    @Test
    void testStringListToDomain() {
        StringListProto proto = CollectionTestObjecsCreator.createStringListProto();
        StringListDomain domain = ProtoDomainConverter.toDomain(proto);
        StringListDomain expected = CollectionTestObjecsCreator.createStringListDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testMessageListToProto() {
        MessageListDomain domain = CollectionTestObjecsCreator.createMessageListDomain();
        MessageListProto proto = ProtoDomainConverter.toProto(domain);
        MessageListProto expected = CollectionTestObjecsCreator.createMessageListProto();

        assertEquals(expected, proto);
    }

    @Test
    void testMessageListToDomain() {
        MessageListProto proto = CollectionTestObjecsCreator.createMessageListProto();
        MessageListDomain domain = ProtoDomainConverter.toDomain(proto);
        MessageListDomain expected = CollectionTestObjecsCreator.createMessageListDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyMessageListToProto() {
        MessageListDomain domain = new MessageListDomain();
        MessageListProto proto = ProtoDomainConverter.toProto(domain);
        MessageListProto expected =
                MessageListProto.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testConcreteMessageListToDomain() {
        ConcreteMessageListProto proto = CollectionTestObjecsCreator.createConcreteMessageListProto();
        ConcreteMessageList domain = ProtoDomainConverter.toDomain(proto);
        ConcreteMessageList expected = CollectionTestObjecsCreator.createConcreteMessageListDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testPrimitiveMapToProto() {
        PrimitiveMap domain = CollectionTestObjecsCreator.createPrimitiveMapDomain();
        PrimitiveMapProto proto = ProtoDomainConverter.toProto(domain);
        PrimitiveMapProto expected = CollectionTestObjecsCreator.createPrimitiveMapProto();

        assertEquals(expected, proto);
    }

    @Test
    void testPrimitiveMapToDomain() {
        PrimitiveMapProto proto = CollectionTestObjecsCreator.createPrimitiveMapProto();
        PrimitiveMap domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveMap expected = CollectionTestObjecsCreator.createPrimitiveMapDomain();

        assertEquals(expected, domain);
    }

    @Test
    void testEmptyPrimitiveMapToProto() {
        PrimitiveMap domain = new PrimitiveMap();
        PrimitiveMapProto proto = ProtoDomainConverter.toProto(domain);
        PrimitiveMapProto expected = PrimitiveMapProto.newBuilder().build();

        assertEquals(expected, proto);
    }

    @Test
    void testEmptyPrimitiveMapToDomain() {
        PrimitiveMapProto proto = PrimitiveMapProto.newBuilder().build();
        PrimitiveMap domain = ProtoDomainConverter.toDomain(proto);
        PrimitiveMap expected = new PrimitiveMap();
        expected.setPrimitiveMap(Collections.emptyMap());

        assertEquals(expected, domain);
    }

}
