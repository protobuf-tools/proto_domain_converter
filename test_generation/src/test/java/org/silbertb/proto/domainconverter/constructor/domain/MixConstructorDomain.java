package org.silbertb.proto.domainconverter.constructor.domain;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConstructor;
import org.silbertb.proto.domainconverter.test.proto.constructor.MixConstructorProto;

@ProtoClass(protoClass = MixConstructorProto.class, blacklist = true)
@Data
public class MixConstructorDomain {

    private final int intVal;
    private String strVal;

    @ProtoConstructor
    public MixConstructorDomain(int intVal) {
        this.intVal = intVal;
    }
}
