package org.silbertb.proto.domainconverter.oneof.domain.one_impl_several_fields.domain;

import lombok.Data;

@Data
public class StringWrapperPojo implements OneofFieldsBase {
    private String strVal;
}
