package org.silbertb.proto.domainconverter.oneof.domain.converter.onfield.field;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.oneof.domain.converter.onfield.IntBigIntConverter;

import java.math.BigInteger;

@Data
public class OneofBigIntFieldImpl implements OneofBaseFieldWithConverter{
    @ProtoConverter(converter = IntBigIntConverter.class)
    @ProtoField(protoName = "int_val")
    private BigInteger bigIntVal;
}
