package org.silbertb.proto.domainconverter.custom_converter.domain;

import org.silbertb.proto.domainconverter.custom.TypeConverter;

import java.util.UUID;

public class UUIDConverter implements TypeConverter<UUID, String> {
    @Override
    public UUID toDomainValue(String protoValue) {
        try {
            return UUID.fromString(protoValue);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Override
    public boolean shouldAssignToProto(UUID domainValue) {
        return domainValue != null;
    }

    @Override
    public String toProtobufValue(UUID domainValue) {
        return domainValue.toString();
    }
}
