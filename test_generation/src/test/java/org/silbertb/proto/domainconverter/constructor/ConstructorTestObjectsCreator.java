package org.silbertb.proto.domainconverter.constructor;

import org.silbertb.proto.domainconverter.constructor.domain.ConstructorDomain;
import org.silbertb.proto.domainconverter.constructor.domain.MixConstructorDomain;
import org.silbertb.proto.domainconverter.test.proto.constructor.ConstructorProto;
import org.silbertb.proto.domainconverter.test.proto.constructor.MixConstructorProto;

public class ConstructorTestObjectsCreator {

    public static ConstructorDomain createConstructorDomain() {
        return new ConstructorDomain(1, "a");
    }

    public static ConstructorProto createConstructorProto() {
        return ConstructorProto.newBuilder()
                .setIntVal(1)
                .setStrVal("a")
                .build();
    }

    public static MixConstructorDomain createMixConstructorDomain() {
        MixConstructorDomain domain = new MixConstructorDomain(1);
        domain.setStrVal("a");

        return domain;
    }

    public static MixConstructorProto createMixConstructorProto() {
        return MixConstructorProto.newBuilder()
                .setIntVal(1)
                .setStrVal("a")
                .build();
    }
}
