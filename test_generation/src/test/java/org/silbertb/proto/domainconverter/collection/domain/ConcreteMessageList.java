package org.silbertb.proto.domainconverter.collection.domain;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import lombok.Data;
import org.silbertb.proto.domainconverter.basic.domain.PrimitiveDomain;
import org.silbertb.proto.domainconverter.test.proto.collection.ConcreteMessageListProto;

import java.util.LinkedList;

@Data
@ProtoClass(protoClass = ConcreteMessageListProto.class)
public class ConcreteMessageList {

    @ProtoField
    private LinkedList<PrimitiveDomain> messageList;
}
