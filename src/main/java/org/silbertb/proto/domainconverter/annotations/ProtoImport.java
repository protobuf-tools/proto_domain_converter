package org.silbertb.proto.domainconverter.annotations;

import java.lang.annotation.*;

/**
 * Import a generated proto-domain converter class, so generated code would be able to use its "toDomain" and "toProto" methods
 */
@Repeatable(ProtoImports.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoImport {
    Class<?> value();
    boolean transitive() default true;
}
