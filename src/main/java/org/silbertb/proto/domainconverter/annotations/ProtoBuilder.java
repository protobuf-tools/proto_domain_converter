package org.silbertb.proto.domainconverter.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.CONSTRUCTOR})
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoBuilder {
    String builderMethodName() default "builder";
    String buildMethodName() default "build";
    String setterPrefix() default "";
}
