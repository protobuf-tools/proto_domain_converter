package org.silbertb.proto.domainconverter.annotations;

import com.google.protobuf.ProtocolMessageEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoEnum {
    Class<? extends ProtocolMessageEnum> protoEnum();
    boolean mapUnrecognized() default false;
}
