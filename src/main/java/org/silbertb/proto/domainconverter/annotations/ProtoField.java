package org.silbertb.proto.domainconverter.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Map between a field or constructor parameter in the domain class to a field in a protobuf message
 */
@Target(value = {ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoField {
    /**
     * The name of the protobuf field. By default, the name is derived from the name of the domain field/parameter
     * @return The name of the protobuf field
     */
    String protoName() default "";
}
