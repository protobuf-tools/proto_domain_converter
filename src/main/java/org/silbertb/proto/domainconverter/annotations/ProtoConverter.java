package org.silbertb.proto.domainconverter.annotations;

import org.silbertb.proto.domainconverter.custom.ProtoType;
import org.silbertb.proto.domainconverter.custom.TypeConverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declare a converter between a protobuf field and a domain field
 */
@Target(value = {ElementType.FIELD, ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoConverter {
    /**
     * @return The converter
     */
    @SuppressWarnings("rawtypes")
    Class<? extends TypeConverter> converter();

    @Deprecated
    ProtoType protoType() default ProtoType.OTHER;
}
