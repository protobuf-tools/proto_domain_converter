package org.silbertb.proto.domainconverter.annotations;

import com.google.protobuf.Message;
import org.silbertb.proto.domainconverter.custom.Mapper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoClassMapper {
    Class<? extends Mapper<?, ? extends Message>> mapper();
}
