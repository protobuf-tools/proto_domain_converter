package org.silbertb.proto.domainconverter.custom;

import com.google.protobuf.Message;

/**
 * Convert between a domain class to a protobuf generated class.
 * Used with conjunction of {@link org.silbertb.proto.domainconverter.annotations.ProtoClass}
 * @param <T> The protobuf generated class
 * @param <E> The domain class
 */
public interface Mapper<T, E extends Message> {
    T toDomain(E protoValue);
    E toProto(T domainValue);
}
