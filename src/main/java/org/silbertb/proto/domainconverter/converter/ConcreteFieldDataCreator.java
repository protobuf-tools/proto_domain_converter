package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.annotations.ProtoIgnore;
import org.silbertb.proto.domainconverter.conversion_data.ConcreteFieldData;
import org.silbertb.proto.domainconverter.conversion_data.ConfigurationData;
import org.silbertb.proto.domainconverter.conversion_data.FieldType;
import org.silbertb.proto.domainconverter.custom.ProtoType;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;

import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConcreteFieldDataCreator {

    private final LangModelUtil langModelUtil;
    private final ProtoTypeUtil protoTypeUtil;
    private final ConverterLogger logger;
    private final ConfigurationData configurationData;
    private final Set<String> domainFieldsWithGlobalMappers;

    public ConcreteFieldDataCreator(LangModelUtil langModelUtil, ProtoTypeUtil protoTypeUtil, ConverterLogger logger,
                                    ConfigurationData configurationData, Set<String> domainFieldsWithGlobalMappers) {
        this.langModelUtil = langModelUtil;
        this.protoTypeUtil = protoTypeUtil;
        this.logger = logger;
        this.configurationData = configurationData;
        this.domainFieldsWithGlobalMappers = domainFieldsWithGlobalMappers;
    }

    public ConcreteFieldData createFieldData(VariableElement field, boolean blacklist) {
        ProtoField protoFieldAnnotation = field.getAnnotation(ProtoField.class);
        ProtoIgnore protoIgnoreAnnotation = field.getAnnotation(ProtoIgnore.class);
        if (protoIgnoreAnnotation != null || (!blacklist && protoFieldAnnotation == null)) {
            if (protoFieldAnnotation != null) {
                logger.info("Both @ProtoField and @ProtoIgnore annotate " + field.getSimpleName() + ". Ignoring");
            }
            return null;
        }

        String explicitProtoName = protoFieldAnnotation == null ? "" : protoFieldAnnotation.protoName();
        return createFieldData(field, explicitProtoName);
    }

    public ConcreteFieldData createFieldData(VariableElement field, String explicitProtoFieldName) {
        TypeMirror domainType = field.asType();
        ConcreteFieldData.ConcreteFieldDataBuilder fieldData = ConcreteFieldData.builder();
        Set<String> knownMessages = Stream.concat(
                        configurationData.domainClassToConverter().keySet().stream(),
                        domainFieldsWithGlobalMappers.stream())
                .collect(Collectors.toSet());

        FieldType fieldType = protoTypeUtil.calculateFieldType(domainType, knownMessages);
        TypeMirror elementType = protoTypeUtil.getElementType(domainType, fieldType);
        String generatedConverter = getGeneratedConverter(fieldType, domainType, elementType);

        fieldData.domainFieldName(field.getSimpleName().toString())
                .domainTypeFullName(domainType.toString())
                .domainItemTypeFullName(elementType == null ? null : elementType.toString())
                .explicitProtoFieldName(explicitProtoFieldName)
                .fieldType(fieldType)
                .dataStructureConcreteType(protoTypeUtil.calculateDataStructureConcreteType(field))
                .generatedConverter(configurationData.getConverterName(generatedConverter));

        ProtoConverter protoConverterAnnotation = field.getAnnotation(ProtoConverter.class);
        if (protoConverterAnnotation != null) {
            @SuppressWarnings("ResultOfMethodCallIgnored")
            TypeMirror converterType = langModelUtil.getClassFromAnnotation(protoConverterAnnotation::converter);
            ProtoType protoType = protoTypeUtil.getProtoTypeFromConverter(converterType);
            fieldData.protoTypeForConverter(protoType)
                    .converterFullName(converterType.toString());
        }

        return fieldData.build();
    }

    private String getGeneratedConverter(FieldType fieldType, TypeMirror domainType, TypeMirror elementType) {
        switch (fieldType) {
            case MESSAGE_LIST:
            case MAP_TO_MESSAGE:
                return elementType.toString();
            default:
                return domainType.toString();
        }
    }

}
