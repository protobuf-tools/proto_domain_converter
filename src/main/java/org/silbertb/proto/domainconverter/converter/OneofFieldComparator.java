package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.OneofField;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.TypeHierarchyComparator;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeMirror;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.Map;

public class OneofFieldComparator implements Comparator<OneofField> {

    private final TypeHierarchyComparator typeHierarchyComparator;
    private final LangModelUtil langModelUtil;
    private final Map<OneofField, TypeMirror> domainClassByOneofField;

    public OneofFieldComparator(ProcessingEnvironment processingEnv, LangModelUtil langModelUtil) {
        this.typeHierarchyComparator = new TypeHierarchyComparator(processingEnv.getTypeUtils());
        this.langModelUtil = langModelUtil;
        this.domainClassByOneofField = new IdentityHashMap<>();
    }

    @Override
    public int compare(OneofField a, OneofField b) {
        return typeHierarchyComparator.compare(getType(a), getType(b));
    }

    public TypeMirror getType(OneofField oneofField) {
        //noinspection ResultOfMethodCallIgnored
        return domainClassByOneofField.computeIfAbsent(
                oneofField,
                oneofField1 -> langModelUtil.getClassFromAnnotation(oneofField1::domainClass));
    }
}
