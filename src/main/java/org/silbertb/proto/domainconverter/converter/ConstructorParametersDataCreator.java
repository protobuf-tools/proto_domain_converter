package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.OneofBase;
import org.silbertb.proto.domainconverter.annotations.ProtoField;
import org.silbertb.proto.domainconverter.conversion_data.ConcreteFieldData;
import org.silbertb.proto.domainconverter.conversion_data.FieldData;
import org.silbertb.proto.domainconverter.conversion_data.OneofBaseFieldData;

import javax.lang.model.element.*;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ConstructorParametersDataCreator {

    private final ConcreteFieldDataCreator concreteFieldDataCreator;
    private final OneofBaseDataCreator oneofBaseDataCreator;

    public ConstructorParametersDataCreator(ConcreteFieldDataCreator concreteFieldDataCreator, OneofBaseDataCreator oneofBaseDataCreator) {
        this.concreteFieldDataCreator = concreteFieldDataCreator;
        this.oneofBaseDataCreator = oneofBaseDataCreator;
    }

    public <A extends Annotation> List<FieldData> getConstructorParametersData(final TypeElement domainElement, Class<A> constructorAnnotationClass) {
        return getConstructorParametersData(domainElement, constructorAnnotationClass, null);
    }

    public <A extends Annotation> List<FieldData> getConstructorParametersData(final TypeElement domainElement, Class<A> constructorAnnotationClass, Modifier modifier) {
        List<Element> constructors = domainElement.getEnclosedElements().stream()
                .filter(e -> e.getKind().equals(ElementKind.CONSTRUCTOR) && (modifier == null || e.getModifiers().contains(modifier)))
                .collect(Collectors.toList());
        List<FieldData> constructorParameters = null;
        for(Element constructor : constructors) {
            A protoConstructorAnnotation = constructor.getAnnotation(constructorAnnotationClass);
            if(protoConstructorAnnotation == null) {
                continue;
            }

            if(constructorParameters != null) {
                throw new IllegalArgumentException("More than one constructors are annotated with @ProtoConstructor. class: " + domainElement);
            }

            constructorParameters = getConstructorParams((ExecutableElement) constructor);
        }

        return constructorParameters;
    }

    private List<FieldData> getConstructorParams(ExecutableElement constructor) {
        List<FieldData> constructorParameters = new ArrayList<>();
        for(VariableElement param : constructor.getParameters()) {
            ProtoField protoFieldAnnotation = param.getAnnotation(ProtoField.class);
            OneofBase oneofBaseAnnotation = param.getAnnotation(OneofBase.class);
            if(protoFieldAnnotation != null && oneofBaseAnnotation != null) {
                throw new IllegalArgumentException("constructor parameter is annotated with both 'ProtoField' and 'OneofField'. param: " + param);
            }

            if(oneofBaseAnnotation == null) {
                String explicitProtoFieldName = protoFieldAnnotation == null ? "" : protoFieldAnnotation.protoName();
                ConcreteFieldData concreteFieldData = concreteFieldDataCreator.createFieldData(param, explicitProtoFieldName);

                FieldData parameterData =
                        FieldData.builder().concreteFieldData(concreteFieldData).build();
                constructorParameters.add(parameterData);
            } else {
                OneofBaseFieldData oneofBaseFieldData = oneofBaseDataCreator.createOneofBaseFieldData(param);
                FieldData parameterData =
                        FieldData.builder().oneofFieldData(oneofBaseFieldData).build();
                constructorParameters.add(parameterData);
            }
        }

        if(constructorParameters.size() > 0) {
            FieldData lastElement = constructorParameters.get(constructorParameters.size()-1);
            FieldData markedLastElement = FieldData.builder()
                    .concreteFieldData(lastElement.concreteFieldData())
                    .oneofFieldData(lastElement.oneofFieldData())
                    .isLast(true).build();
            constructorParameters.set(constructorParameters.size()-1, markedLastElement);
        }

        return constructorParameters;
    }
}
