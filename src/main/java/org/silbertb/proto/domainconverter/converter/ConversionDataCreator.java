package org.silbertb.proto.domainconverter.converter;

import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoEnum;
import org.silbertb.proto.domainconverter.annotations.ProtoGlobalMapper;
import org.silbertb.proto.domainconverter.conversion_data.*;
import org.silbertb.proto.domainconverter.util.LangModelUtil;
import org.silbertb.proto.domainconverter.util.ProtoTypeUtil;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConversionDataCreator {

    private final ProcessingEnvironment processingEnv;
    private final LangModelUtil langModelUtil;
    private final ConverterLogger logger;

    public ConversionDataCreator(ProcessingEnvironment processingEnv,
                                 ConverterLogger logger) {
        this.processingEnv = processingEnv;
        this.logger = logger;
        this.langModelUtil = new LangModelUtil(processingEnv);
    }

    public ConversionData createConversionData(RoundEnvironment roundEnv) {
        LangModelUtil langModelUtil = new LangModelUtil(processingEnv);
        ProtoTypeUtil protoTypeUtil = new ProtoTypeUtil(processingEnv, langModelUtil);

        ConfigurationCreator configurationCreator = new ConfigurationCreator(processingEnv, langModelUtil);
        ConfigurationData configurationData = configurationCreator.createConfigurationData(roundEnv);
        List<GlobalMapperInfo> globalMappers = findGlobalMappers(roundEnv);
        List<GlobalMapperInfo> predefinedGlobalMappers = getPredefinedGlobalMappers();

        Set<String> domainFieldsWithGlobalMappers =
                Stream.concat(globalMappers.stream(), predefinedGlobalMappers.stream())
                        .map(GlobalMapperInfo::getDomainName)
                        .collect(Collectors.toSet());
        domainFieldsWithGlobalMappers.forEach(domainField -> configurationData.domainClassToConverter().remove(domainField));

        ClassDataCreator classDataCreator = new ClassDataCreator(
                langModelUtil,
                processingEnv,
                protoTypeUtil,
                logger,
                configurationData,
                domainFieldsWithGlobalMappers
        );

        ConversionData.ConversionDataBuilder conversionData = ConversionData.builder()
                .generator(this.getClass().getName())
                .converterFullName(configurationData.generatedConverterName());

        List<EnumData> enumDataList = createEnumData(roundEnv);

        Map<String, List<ClassData>> protoToDomainClasses = createClassesDataFromProtoClass(roundEnv, classDataCreator);
        Map<String, List<ClassData>> protoToDomainClassFromGlobalMappers = createClassesData(globalMappers, classDataCreator);
        mergeClassDataMap(protoToDomainClasses, protoToDomainClassFromGlobalMappers, false);

        Map<String, List<ClassData>> protoToDomainClassFromPredefinedGlobalMappers = createClassesData(predefinedGlobalMappers, classDataCreator);
        mergeClassDataMap(protoToDomainClasses, protoToDomainClassFromPredefinedGlobalMappers, true);

        List<ClassData> defaultDomainClasses = getDefaultDomainClasses(protoToDomainClasses);
        List<EnumData> defaultEnumsData = getDefaultDomainClasses(
                enumDataList.stream().collect(Collectors.groupingBy(EnumData::protoFullName)));

        return conversionData
                .configurationData(configurationData)
                .classesData(protoToDomainClasses.values().stream().flatMap(Collection::stream).collect(Collectors.toUnmodifiableList()))
                .defaultClassesData(defaultDomainClasses)
                .enumData(enumDataList)
                .defaultEnumsData(defaultEnumsData)
                .build();
    }

    private List<GlobalMapperInfo> getPredefinedGlobalMappers() {
        return getPredefinedGlobalMappers("org.silbertb.proto.domainconverter.predefined_mappers.InstantTimestampMapper");
    }

    private List<GlobalMapperInfo> getPredefinedGlobalMappers(String... classNames) {
        GlobalMapperTypesCreator creator = new GlobalMapperTypesCreator(langModelUtil);
        return Arrays.stream(classNames)
                .map(className -> processingEnv.getElementUtils().getTypeElement(className))
                .map(creator::create)
                .collect(Collectors.toList());
    }

    private void mergeClassDataMap(Map<String, List<ClassData>> classDataFromProtoClass,
                                   Map<String, List<ClassData>> classDataFromGlobalMapper,
                                   boolean ignoreIfExists) {
        for (Map.Entry<String, List<ClassData>> e : classDataFromGlobalMapper.entrySet()) {
            List<ClassData> merged = mergeClassDataLists(classDataFromProtoClass.get(e.getKey()), e.getValue(), ignoreIfExists);
            classDataFromProtoClass.put(e.getKey(), merged);
        }
    }

    private List<ClassData> mergeClassDataLists(List<ClassData> l1, List<ClassData> l2, boolean ignoreIfExists) {
        Map<String, ClassData> domainClassToClassData = new HashMap<>();
        if (l1 != null) {
            for (ClassData classData : l1) {
                domainClassToClassData.put(classData.domainFullName(), classData);
            }
        }

        if (l2 != null) {
            for (ClassData classData : l2) {
                if (domainClassToClassData.containsKey(classData.domainFullName())) {
                    if (!ignoreIfExists) {
                        throw new IllegalArgumentException("Both @ProtoClass and @ProtoGlobalMapper are defined to the same proto message and domain class. proto: "
                                + classData.protoFullName() + " domain: " + classData.domainFullName());
                    }
                } else {
                    domainClassToClassData.put(classData.domainFullName(), classData);
                }
            }
        }

        return new ArrayList<>(domainClassToClassData.values());
    }

    private List<GlobalMapperInfo> findGlobalMappers(RoundEnvironment roundEnv) {
        GlobalMapperTypesCreator creator = new GlobalMapperTypesCreator(langModelUtil);
        return roundEnv.getElementsAnnotatedWith(ProtoGlobalMapper.class).stream()
                .map(e -> creator.create((TypeElement) e))
                .collect(Collectors.toList());
    }

    private Map<String, List<ClassData>> createClassesData(List<GlobalMapperInfo> globalMappersInfo, ClassDataCreator classDataCreator) {
        return globalMappersInfo.stream()
                .map(classDataCreator::createClassData)
                .collect(Collectors.groupingBy(ClassData::protoFullName));
    }

    private Map<String, List<ClassData>> createClassesDataFromProtoClass(RoundEnvironment roundEnv, ClassDataCreator classDataCreator) {
        return roundEnv.getElementsAnnotatedWith(ProtoClass.class).stream()
                .map(element -> (TypeElement) element)
                .map(classDataCreator::createClassData)
                .collect(Collectors.groupingBy(ClassData::protoFullName));

    }

    private <T extends DataWithDefault> List<T> getDefaultDomainClasses(Map<String, List<T>> protoToDomainClass) {
        return protoToDomainClass.values().stream()
                .map(this::getDefault)
                .flatMap(Optional::stream)
                .collect(Collectors.toUnmodifiableList());
    }

    private <T extends DataWithDefault> Optional<T> getDefault(List<T> dataWithDefault) {
        if (dataWithDefault.size() == 1) {
            return Optional.of(dataWithDefault.get(0));
        }
        List<T> defaults = dataWithDefault.stream().filter(T::isDefault).collect(Collectors.toUnmodifiableList());
        if (defaults.size() > 1) {
            throw new IllegalArgumentException("More than one default class exists for proto '" + dataWithDefault.get(0).domainFullName() + "'.");
        } else if (defaults.size() == 1) {
            return Optional.of(defaults.get(0));
        }
        // More than one domain class, but none are marked as default.
        return Optional.empty();
    }

    private List<EnumData> createEnumData(RoundEnvironment roundEnv) {
        EnumDataCreator enumDataCreator = new EnumDataCreator(langModelUtil, processingEnv, logger);
        return roundEnv.getElementsAnnotatedWith(ProtoEnum.class).stream()
                .map(element -> (TypeElement) element)
                .map(enumDataCreator::createEnumData)
                .collect(Collectors.toList());
    }

}
