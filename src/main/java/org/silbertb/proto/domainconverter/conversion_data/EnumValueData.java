package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class EnumValueData {
    String domainName;
    String protoName;
}
