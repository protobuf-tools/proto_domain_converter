package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class OneofBaseClassData {
    String oneofProtoName;
    List<OneofFieldDataForClass> oneOfFieldsData;
}
