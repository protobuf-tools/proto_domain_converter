package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Accessors;

import java.util.List;

@Accessors(fluent = true)
@Value
@Builder
public class OneofBaseFieldData {
    String domainFieldType;
    String domainFieldName;
    String oneofBaseField;
    String oneofProtoName;
    List<OneofFieldData> oneOfFieldsData;
}
