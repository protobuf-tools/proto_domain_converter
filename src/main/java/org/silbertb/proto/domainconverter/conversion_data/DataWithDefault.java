package org.silbertb.proto.domainconverter.conversion_data;

public interface DataWithDefault {
    boolean isDefault();
    String domainFullName();
}
