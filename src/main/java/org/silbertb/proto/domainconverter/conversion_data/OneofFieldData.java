package org.silbertb.proto.domainconverter.conversion_data;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import org.silbertb.proto.domainconverter.util.StringUtils;

@Accessors(fluent = true)
@Data
@Builder
public class OneofFieldData {
    private final String protoField;
    private final String oneofImplClass;
    private final String converterFullName;
    private final ConcreteFieldData constructorParameter;
    private final ConcreteFieldData fieldData;
    private final BuilderData builderData;

    public String oneofFieldCase() {
        return protoField.toUpperCase();
    }

    public String oneOfProtoField() {
        return StringUtils.snakeCaseToPascalCase(protoField);
    }

    public String oneofImplVariable() {
        String oneofImplClassSimple = StringUtils.getSimpleName(oneofImplClass);
        return StringUtils.pascalCaseToCamelCase(oneofImplClassSimple);
    }

    public boolean mapToDomainField() {
        return fieldData != null || constructorParameter != null;
    }

    public boolean hasConverter() {
        return converterFullName != null;
    }

    public String converterVariable() {
        String converterSimpleName = StringUtils.getSimpleName(converterFullName);
        return StringUtils.pascalCaseToCamelCase(converterSimpleName);
    }
}
