package silbertb.protodomainconverter.dep1;

import org.silbertb.proto.domainconverter.annotations.ProtoConfigure;

@ProtoConfigure(converterName = "silbertb.protodomainconverter.dep1.Dep1Converter")
public class Dep1Configuration {
}
