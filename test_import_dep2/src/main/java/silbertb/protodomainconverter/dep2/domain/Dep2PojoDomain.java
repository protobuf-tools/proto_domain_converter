package silbertb.protodomainconverter.dep2.domain;

import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.model.proto.Dep2PojoProto;
import silbertb.protodomainconverter.dep1.domain.Dep1Domain;

import java.util.List;
import java.util.Map;

@Data
@ProtoClass(protoClass = Dep2PojoProto.class, blacklist = true)
public class Dep2PojoDomain {
    private int intValue;
    private Dep1Domain dep1;
    private List<Dep1Domain> dep1List;
    private Map<String, Dep1Domain> dep1Map;


}
