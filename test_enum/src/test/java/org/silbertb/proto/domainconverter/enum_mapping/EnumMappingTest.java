package org.silbertb.proto.domainconverter.enum_mapping;

import org.junit.jupiter.api.Test;
import org.silbertb.proto.domainconverter.enum_mapping.domain.ClassWithEnumDomain;
import org.silbertb.proto.domainconverter.enum_mapping.domain.EnumDomain;
import org.silbertb.proto.domainconverter.enum_mapping.domain.Place1Domain;
import org.silbertb.proto.domainconverter.enum_mapping.domain.Place2Domain;
import org.silbertb.proto.domainconverter.generated.ProtoDomainConverter;
import org.silbertb.proto.domainconverter.test.proto.enummapping.ClassWithEnumProto;
import org.silbertb.proto.domainconverter.test.proto.enummapping.EnumProto;
import org.silbertb.proto.domainconverter.test.proto.enummapping.PlaceProto;

import static org.junit.jupiter.api.Assertions.*;


public class EnumMappingTest {

    @Test
    public void testDefaultEnumMappingToDomain() {
        EnumDomain actual = ProtoDomainConverter.toDomain(EnumProto.SECOND);
        assertEquals(EnumDomain.SECOND, actual);
    }

    @Test
    public void testDefaultEnumMappingToProto() {
        EnumProto actual = ProtoDomainConverter.toProto(EnumDomain.SECOND);
        assertEquals(EnumProto.SECOND, actual);
    }

    @Test
    public void testEnumMappingDifferentNameToDomain() {
        EnumDomain actual = ProtoDomainConverter.toDomain(EnumProto.FIRST);
        assertEquals(EnumDomain.MY_FIRST, actual);
    }

    @Test
    public void testEnumMappingDifferentNameToProto() {
        EnumProto actual = ProtoDomainConverter.toProto(EnumDomain.MY_FIRST);
        assertEquals(EnumProto.FIRST, actual);
    }

    @Test
    public void testClassWithEnumDomainToDomain() {
        ClassWithEnumProto proto = ClassWithEnumProto.newBuilder()
                .setColor(ClassWithEnumProto.ColorProto.BLUE)
                //'EnumDomain' default is 'FIRST'
                .build();
        ClassWithEnumDomain actual = ProtoDomainConverter.toDomain(proto);

        ClassWithEnumDomain expected = ClassWithEnumDomain.builder()
                .color(ClassWithEnumDomain.ColorDomain.BLUE)
                .e(EnumDomain.MY_FIRST)
                .build();

        assertEquals(expected, actual);
    }

    @Test
    public void testMultiDomainToDomainWithMarker() {
        Place1Domain actual = ProtoDomainConverter.toDomain(PlaceProto.HERE, (Place1Domain)null);
        assertEquals(Place1Domain.HERE, actual);
    }

    @Test
    public void testMultiDomainToDomainDefauld() {
        Place2Domain actual = ProtoDomainConverter.toDomain(PlaceProto.HERE);
        assertEquals(Place2Domain.HERE, actual);
    }
}
